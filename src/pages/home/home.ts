import { Component } from '@angular/core';
import { NavController, AlertController, Platform } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  tarefas: Array<{status: boolean, descricao: any}> = [];

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    private storage: NativeStorage,
    public platform: Platform,
  ) {

    this.platform.ready().then((readySource) => {
      this.loadTarefas();
    });

  }

  itemTapped(i) {
    this.tarefas[i].status = !this.tarefas[i].status;
    this.saveTarefas();
  }

  itemRemove(i) {
    this.tarefas.splice(i, 1);
    this.saveTarefas();
  }

  getLabelClass(status) {
    return status ? 'concluido' : '';
  }

  saveTarefas(){

    this.storage.setItem('tarefas', JSON.stringify(this.tarefas));

  }

  loadTarefas(){

    this.storage.getItem('tarefas').then((val) => {
      this.tarefas = JSON.parse(val);
    });

  }

  tarefaShowPrompt() {
    const prompt = this.alertCtrl.create({
      title: 'Nova Tarefa',
      message: "Entre com a descrição da sua nova tarefa",
      inputs: [
        {
          name: 'tarefa',
          placeholder: 'Tarefa'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Adicionar',
          handler: data => {

            this.tarefas.push( {status: false, descricao: data.tarefa} );
            this.saveTarefas();

          }
        }
      ]
    });
    prompt.present();
  }

}
